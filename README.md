Role Name
=========

Setup NGINX as a reverse proxy for docker containers

Requirements
------------

None

Role Variables
--------------

- `domain` - domain to accept requests on
- `port` - port for service to connect to
- `ip_address` - ip_address of the service to proxy too


Dependencies
------------

None

Example Playbook
----------------

```                                                           
role:                                                         
    - { role: 'nginx-container-proxy', domain: 'example.com', port: 444 } 
                                                              
```                                                           

License
-------

BSD

Author Information
------------------

See https://ofmax.li
